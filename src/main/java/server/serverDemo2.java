package server;

import com.derbysoft.servreg.rpc.products.*;
import io.grpc.Server;
import io.grpc.ServerBuilder;
import io.grpc.stub.StreamObserver;

import java.io.IOException;

public class serverDemo2 {
    // 定义一个Server对象，监听端口来获取rpc请求，以进行下面的处理
    private Server server;

    //使用main方法来测试server端
    public static void main(String[] args) throws IOException, InterruptedException {

        final serverDemo2 serverDemo2 = new serverDemo2();

        //启动server
        serverDemo2.start();

        //block 一直到退出程序
        serverDemo2.blockUntilShutdown();

        // 服务注册至注册中心

        //
    }

    /**
     * 启动一个Server实例，监听client端的请求并处理
     * @throws IOException
     */
    private void start() throws IOException {

        //server运行在的端口号
        int port = 50052;

        // 给server添加监听端口号，添加 包含业务处理逻辑的类，然后启动
        server = ServerBuilder.forPort(port)
                .addService(new serverDemo2.ConnectivityProductsImpl())
                .build()
                .start();

    }

    /**
     * 阻塞server直到关闭程序
     * @throws InterruptedException
     */
    private void blockUntilShutdown() throws InterruptedException {

        if (server != null) {
            server.awaitTermination();
        }

    }


    /**
     * proto文件被编译后，在生成的HelloGrpc的抽象内部类HelloImplBase中包含了 proto中定义的服务接口的简单实现
     * 该HelloImpl类需要重写这些方法，添加需要的处理逻辑
     */
    static class ConnectivityProductsImpl extends ConnectivityProductsGrpc.ConnectivityProductsImplBase {

        // proto文件中的sayHello服务接口被编译后，在生成的HelloGrpc的抽象内部类HelloImplBase中有一个简单的实现
        // 因此，在server端需要重写这个方法，添加上相应的逻辑
        @Override
        public void getProducts(GetProductsRequest req, StreamObserver<GetProductsResponse> responseObserver) {

            System.out.println("server2 端接收 request uri:"+req.getHeader().getHeader().getUri()+"channelCode:"+req.getProductsRQ().getChannelCode());

            ProductDTO productDTO = ProductDTO.newBuilder().setRoomTypeCode("roomCode2").setRatePlanCode("rateCode2").build();
            ProductsRS productsRS = ProductsRS.newBuilder().addProducts(productDTO).build();


            GetProductsResponse response = GetProductsResponse.newBuilder().setProductsRS(productsRS).build();

            // 调用onNext()方法来通知gRPC框架把reply 从server端 发送回 client端
            responseObserver.onNext(response);

            // 表示完成调用
            responseObserver.onCompleted();
        }
    }
}
