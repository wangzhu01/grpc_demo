// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: products_service.proto

package com.derbysoft.servreg.rpc.products;

public interface ProductDTOOrBuilder extends
    // @@protoc_insertion_point(interface_extends:com.derbysoft.servreg.ProductDTO)
    com.google.protobuf.MessageOrBuilder {

  /**
   * <code>string ratePlanCode = 1;</code>
   * @return The ratePlanCode.
   */
  String getRatePlanCode();
  /**
   * <code>string ratePlanCode = 1;</code>
   * @return The bytes for ratePlanCode.
   */
  com.google.protobuf.ByteString
      getRatePlanCodeBytes();

  /**
   * <code>string roomTypeCode = 2;</code>
   * @return The roomTypeCode.
   */
  String getRoomTypeCode();
  /**
   * <code>string roomTypeCode = 2;</code>
   * @return The bytes for roomTypeCode.
   */
  com.google.protobuf.ByteString
      getRoomTypeCodeBytes();

  /**
   * <code>.com.derbysoft.servreg.TPAExtensionsDTO tpaExtensions = 1000;</code>
   * @return Whether the tpaExtensions field is set.
   */
  boolean hasTpaExtensions();
  /**
   * <code>.com.derbysoft.servreg.TPAExtensionsDTO tpaExtensions = 1000;</code>
   * @return The tpaExtensions.
   */
  TPAExtensionsDTO getTpaExtensions();
  /**
   * <code>.com.derbysoft.servreg.TPAExtensionsDTO tpaExtensions = 1000;</code>
   */
  TPAExtensionsDTOOrBuilder getTpaExtensionsOrBuilder();
}
