// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: products_service.proto

package com.derbysoft.servreg.rpc.products;

public final class ConnectivityProductsPB {
  private ConnectivityProductsPB() {}
  public static void registerAllExtensions(
      com.google.protobuf.ExtensionRegistryLite registry) {
  }

  public static void registerAllExtensions(
      com.google.protobuf.ExtensionRegistry registry) {
    registerAllExtensions(
        (com.google.protobuf.ExtensionRegistryLite) registry);
  }
  static final com.google.protobuf.Descriptors.Descriptor
    internal_static_com_derbysoft_servreg_GetProductsRequest_descriptor;
  static final 
    com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internal_static_com_derbysoft_servreg_GetProductsRequest_fieldAccessorTable;
  static final com.google.protobuf.Descriptors.Descriptor
    internal_static_com_derbysoft_servreg_GetProductsResponse_descriptor;
  static final 
    com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internal_static_com_derbysoft_servreg_GetProductsResponse_fieldAccessorTable;
  static final com.google.protobuf.Descriptors.Descriptor
    internal_static_com_derbysoft_servreg_ProductsRQ_descriptor;
  static final 
    com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internal_static_com_derbysoft_servreg_ProductsRQ_fieldAccessorTable;
  static final com.google.protobuf.Descriptors.Descriptor
    internal_static_com_derbysoft_servreg_ProductsRS_descriptor;
  static final 
    com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internal_static_com_derbysoft_servreg_ProductsRS_fieldAccessorTable;
  static final com.google.protobuf.Descriptors.Descriptor
    internal_static_com_derbysoft_servreg_ProductDTO_descriptor;
  static final 
    com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internal_static_com_derbysoft_servreg_ProductDTO_fieldAccessorTable;
  static final com.google.protobuf.Descriptors.Descriptor
    internal_static_com_derbysoft_servreg_KeyValue_descriptor;
  static final 
    com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internal_static_com_derbysoft_servreg_KeyValue_fieldAccessorTable;
  static final com.google.protobuf.Descriptors.Descriptor
    internal_static_com_derbysoft_servreg_TPAExtensionsDTO_descriptor;
  static final 
    com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internal_static_com_derbysoft_servreg_TPAExtensionsDTO_fieldAccessorTable;
  static final com.google.protobuf.Descriptors.Descriptor
    internal_static_com_derbysoft_servreg_TraceInfo_descriptor;
  static final 
    com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internal_static_com_derbysoft_servreg_TraceInfo_fieldAccessorTable;
  static final com.google.protobuf.Descriptors.Descriptor
    internal_static_com_derbysoft_servreg_ServiceInfo_descriptor;
  static final 
    com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internal_static_com_derbysoft_servreg_ServiceInfo_fieldAccessorTable;
  static final com.google.protobuf.Descriptors.Descriptor
    internal_static_com_derbysoft_servreg_Header_descriptor;
  static final 
    com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internal_static_com_derbysoft_servreg_Header_fieldAccessorTable;
  static final com.google.protobuf.Descriptors.Descriptor
    internal_static_com_derbysoft_servreg_RequestMessage_descriptor;
  static final 
    com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internal_static_com_derbysoft_servreg_RequestMessage_fieldAccessorTable;
  static final com.google.protobuf.Descriptors.Descriptor
    internal_static_com_derbysoft_servreg_ResponseMessage_descriptor;
  static final 
    com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internal_static_com_derbysoft_servreg_ResponseMessage_fieldAccessorTable;
  static final com.google.protobuf.Descriptors.Descriptor
    internal_static_com_derbysoft_servreg_ErrorDTO_descriptor;
  static final 
    com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internal_static_com_derbysoft_servreg_ErrorDTO_fieldAccessorTable;

  public static com.google.protobuf.Descriptors.FileDescriptor
      getDescriptor() {
    return descriptor;
  }
  private static  com.google.protobuf.Descriptors.FileDescriptor
      descriptor;
  static {
    String[] descriptorData = {
      "\n\026products_service.proto\022\025com.derbysoft." +
      "servreg\"\202\001\n\022GetProductsRequest\0225\n\nproduc" +
      "tsRQ\030\001 \001(\0132!.com.derbysoft.servreg.Produ" +
      "ctsRQ\0225\n\006header\030\002 \001(\0132%.com.derbysoft.se" +
      "rvreg.RequestMessage\"\206\001\n\023GetProductsResp" +
      "onse\0225\n\nproductsRS\030\001 \001(\0132!.com.derbysoft" +
      ".servreg.ProductsRS\0228\n\010response\030\002 \001(\0132&." +
      "com.derbysoft.servreg.ResponseMessage\"\261\001" +
      "\n\nProductsRQ\022\023\n\013channelCode\030\001 \001(\t\022\024\n\014sup" +
      "plierCode\030\002 \001(\t\022\021\n\thotelCode\030\003 \001(\t\022\021\n\tra" +
      "tePlans\030\004 \003(\t\022\021\n\troomTypes\030\005 \003(\t\022?\n\rtpaE" +
      "xtensions\030\350\007 \001(\0132\'.com.derbysoft.servreg" +
      ".TPAExtensionsDTO\"\202\001\n\nProductsRS\0223\n\010prod" +
      "ucts\030\001 \003(\0132!.com.derbysoft.servreg.Produ" +
      "ctDTO\022?\n\rtpaExtensions\030\350\007 \001(\0132\'.com.derb" +
      "ysoft.servreg.TPAExtensionsDTO\"y\n\nProduc" +
      "tDTO\022\024\n\014ratePlanCode\030\001 \001(\t\022\024\n\014roomTypeCo" +
      "de\030\002 \001(\t\022?\n\rtpaExtensions\030\350\007 \001(\0132\'.com.d" +
      "erbysoft.servreg.TPAExtensionsDTO\"&\n\010Key" +
      "Value\022\013\n\003key\030\001 \001(\t\022\r\n\005value\030\002 \001(\t\"E\n\020TPA" +
      "ExtensionsDTO\0221\n\010elements\030\001 \003(\0132\037.com.de" +
      "rbysoft.servreg.KeyValue\"\307\001\n\tTraceInfo\022\017" +
      "\n\007traceId\030\001 \001(\003\022\016\n\006spanId\030\002 \001(\003\022\024\n\014paren" +
      "tSpanId\030\003 \001(\003\022\017\n\007sampled\030\004 \001(\010\022\r\n\005flags\030" +
      "\005 \001(\005\022\020\n\010peerHost\030\006 \001(\t\022\020\n\010peerPort\030\007 \001(" +
      "\005\022?\n\rtpaExtensions\030\350\007 \001(\0132\'.com.derbysof" +
      "t.servreg.TPAExtensionsDTO\":\n\013ServiceInf" +
      "o\022\014\n\004name\030\001 \001(\t\022\014\n\004host\030\002 \001(\t\022\017\n\007version" +
      "\030\003 \001(\t\"\274\002\n\006Header\022\013\n\003uri\030\001 \001(\t\022\016\n\006taskId" +
      "\030\002 \001(\t\022\016\n\006source\030\003 \001(\t\022\023\n\013destination\030\004 " +
      "\001(\t\0220\n\007headers\030\005 \003(\0132\037.com.derbysoft.ser" +
      "vreg.KeyValue\022\017\n\007timeout\030\n \001(\005\0227\n\013invoke" +
      "rInfo\030\013 \001(\0132\".com.derbysoft.servreg.Serv" +
      "iceInfo\0223\n\ttraceInfo\030\024 \001(\0132 .com.derbyso" +
      "ft.servreg.TraceInfo\022?\n\rtpaExtensions\030\350\007" +
      " \001(\0132\'.com.derbysoft.servreg.TPAExtensio" +
      "nsDTO\"\216\001\n\016RequestMessage\022-\n\006header\030\001 \001(\013" +
      "2\035.com.derbysoft.servreg.Header\022\014\n\004body\030" +
      "\n \001(\014\022?\n\rtpaExtensions\030\350\007 \001(\0132\'.com.derb" +
      "ysoft.servreg.TPAExtensionsDTO\"\320\002\n\017Respo" +
      "nseMessage\022\016\n\006taskId\030\001 \001(\t\0225\n\014elapsedTim" +
      "es\030\002 \003(\0132\037.com.derbysoft.servreg.KeyValu" +
      "e\022.\n\005error\030\003 \001(\0132\037.com.derbysoft.servreg" +
      ".ErrorDTO\0227\n\013replierInfo\030\004 \001(\0132\".com.der" +
      "bysoft.servreg.ServiceInfo\0220\n\007headers\030\005 " +
      "\003(\0132\037.com.derbysoft.servreg.KeyValue\022\014\n\004" +
      "body\030\n \001(\014\022\014\n\004time\030\017 \001(\003\022?\n\rtpaExtension" +
      "s\030\350\007 \001(\0132\'.com.derbysoft.servreg.TPAExte" +
      "nsionsDTO\"z\n\010ErrorDTO\022\014\n\004code\030\001 \001(\t\022\017\n\007m" +
      "essage\030\002 \001(\t\022\016\n\006source\030\003 \001(\t\022?\n\rtpaExten" +
      "sions\030\350\007 \001(\0132\'.com.derbysoft.servreg.TPA" +
      "ExtensionsDTO2~\n\024ConnectivityProducts\022f\n" +
      "\013GetProducts\022).com.derbysoft.servreg.Get" +
      "ProductsRequest\032*.com.derbysoft.servreg." +
      "GetProductsResponse\"\000B>\n\"com.derbysoft.s" +
      "ervreg.rpc.productsB\026ConnectivityProduct" +
      "sPBP\001b\006proto3"
    };
    descriptor = com.google.protobuf.Descriptors.FileDescriptor
      .internalBuildGeneratedFileFrom(descriptorData,
        new com.google.protobuf.Descriptors.FileDescriptor[] {
        });
    internal_static_com_derbysoft_servreg_GetProductsRequest_descriptor =
      getDescriptor().getMessageTypes().get(0);
    internal_static_com_derbysoft_servreg_GetProductsRequest_fieldAccessorTable = new
      com.google.protobuf.GeneratedMessageV3.FieldAccessorTable(
        internal_static_com_derbysoft_servreg_GetProductsRequest_descriptor,
        new String[] { "ProductsRQ", "Header", });
    internal_static_com_derbysoft_servreg_GetProductsResponse_descriptor =
      getDescriptor().getMessageTypes().get(1);
    internal_static_com_derbysoft_servreg_GetProductsResponse_fieldAccessorTable = new
      com.google.protobuf.GeneratedMessageV3.FieldAccessorTable(
        internal_static_com_derbysoft_servreg_GetProductsResponse_descriptor,
        new String[] { "ProductsRS", "Response", });
    internal_static_com_derbysoft_servreg_ProductsRQ_descriptor =
      getDescriptor().getMessageTypes().get(2);
    internal_static_com_derbysoft_servreg_ProductsRQ_fieldAccessorTable = new
      com.google.protobuf.GeneratedMessageV3.FieldAccessorTable(
        internal_static_com_derbysoft_servreg_ProductsRQ_descriptor,
        new String[] { "ChannelCode", "SupplierCode", "HotelCode", "RatePlans", "RoomTypes", "TpaExtensions", });
    internal_static_com_derbysoft_servreg_ProductsRS_descriptor =
      getDescriptor().getMessageTypes().get(3);
    internal_static_com_derbysoft_servreg_ProductsRS_fieldAccessorTable = new
      com.google.protobuf.GeneratedMessageV3.FieldAccessorTable(
        internal_static_com_derbysoft_servreg_ProductsRS_descriptor,
        new String[] { "Products", "TpaExtensions", });
    internal_static_com_derbysoft_servreg_ProductDTO_descriptor =
      getDescriptor().getMessageTypes().get(4);
    internal_static_com_derbysoft_servreg_ProductDTO_fieldAccessorTable = new
      com.google.protobuf.GeneratedMessageV3.FieldAccessorTable(
        internal_static_com_derbysoft_servreg_ProductDTO_descriptor,
        new String[] { "RatePlanCode", "RoomTypeCode", "TpaExtensions", });
    internal_static_com_derbysoft_servreg_KeyValue_descriptor =
      getDescriptor().getMessageTypes().get(5);
    internal_static_com_derbysoft_servreg_KeyValue_fieldAccessorTable = new
      com.google.protobuf.GeneratedMessageV3.FieldAccessorTable(
        internal_static_com_derbysoft_servreg_KeyValue_descriptor,
        new String[] { "Key", "Value", });
    internal_static_com_derbysoft_servreg_TPAExtensionsDTO_descriptor =
      getDescriptor().getMessageTypes().get(6);
    internal_static_com_derbysoft_servreg_TPAExtensionsDTO_fieldAccessorTable = new
      com.google.protobuf.GeneratedMessageV3.FieldAccessorTable(
        internal_static_com_derbysoft_servreg_TPAExtensionsDTO_descriptor,
        new String[] { "Elements", });
    internal_static_com_derbysoft_servreg_TraceInfo_descriptor =
      getDescriptor().getMessageTypes().get(7);
    internal_static_com_derbysoft_servreg_TraceInfo_fieldAccessorTable = new
      com.google.protobuf.GeneratedMessageV3.FieldAccessorTable(
        internal_static_com_derbysoft_servreg_TraceInfo_descriptor,
        new String[] { "TraceId", "SpanId", "ParentSpanId", "Sampled", "Flags", "PeerHost", "PeerPort", "TpaExtensions", });
    internal_static_com_derbysoft_servreg_ServiceInfo_descriptor =
      getDescriptor().getMessageTypes().get(8);
    internal_static_com_derbysoft_servreg_ServiceInfo_fieldAccessorTable = new
      com.google.protobuf.GeneratedMessageV3.FieldAccessorTable(
        internal_static_com_derbysoft_servreg_ServiceInfo_descriptor,
        new String[] { "Name", "Host", "Version", });
    internal_static_com_derbysoft_servreg_Header_descriptor =
      getDescriptor().getMessageTypes().get(9);
    internal_static_com_derbysoft_servreg_Header_fieldAccessorTable = new
      com.google.protobuf.GeneratedMessageV3.FieldAccessorTable(
        internal_static_com_derbysoft_servreg_Header_descriptor,
        new String[] { "Uri", "TaskId", "Source", "Destination", "Headers", "Timeout", "InvokerInfo", "TraceInfo", "TpaExtensions", });
    internal_static_com_derbysoft_servreg_RequestMessage_descriptor =
      getDescriptor().getMessageTypes().get(10);
    internal_static_com_derbysoft_servreg_RequestMessage_fieldAccessorTable = new
      com.google.protobuf.GeneratedMessageV3.FieldAccessorTable(
        internal_static_com_derbysoft_servreg_RequestMessage_descriptor,
        new String[] { "Header", "Body", "TpaExtensions", });
    internal_static_com_derbysoft_servreg_ResponseMessage_descriptor =
      getDescriptor().getMessageTypes().get(11);
    internal_static_com_derbysoft_servreg_ResponseMessage_fieldAccessorTable = new
      com.google.protobuf.GeneratedMessageV3.FieldAccessorTable(
        internal_static_com_derbysoft_servreg_ResponseMessage_descriptor,
        new String[] { "TaskId", "ElapsedTimes", "Error", "ReplierInfo", "Headers", "Body", "Time", "TpaExtensions", });
    internal_static_com_derbysoft_servreg_ErrorDTO_descriptor =
      getDescriptor().getMessageTypes().get(12);
    internal_static_com_derbysoft_servreg_ErrorDTO_fieldAccessorTable = new
      com.google.protobuf.GeneratedMessageV3.FieldAccessorTable(
        internal_static_com_derbysoft_servreg_ErrorDTO_descriptor,
        new String[] { "Code", "Message", "Source", "TpaExtensions", });
  }

  // @@protoc_insertion_point(outer_class_scope)
}
