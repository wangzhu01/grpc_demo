// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: products_service.proto

package com.derbysoft.servreg.rpc.products;

/**
 * Protobuf type {@code com.derbysoft.servreg.ProductDTO}
 */
public final class ProductDTO extends
    com.google.protobuf.GeneratedMessageV3 implements
    // @@protoc_insertion_point(message_implements:com.derbysoft.servreg.ProductDTO)
    ProductDTOOrBuilder {
private static final long serialVersionUID = 0L;
  // Use ProductDTO.newBuilder() to construct.
  private ProductDTO(com.google.protobuf.GeneratedMessageV3.Builder<?> builder) {
    super(builder);
  }
  private ProductDTO() {
    ratePlanCode_ = "";
    roomTypeCode_ = "";
  }

  @Override
  @SuppressWarnings({"unused"})
  protected Object newInstance(
      UnusedPrivateParameter unused) {
    return new ProductDTO();
  }

  @Override
  public final com.google.protobuf.UnknownFieldSet
  getUnknownFields() {
    return this.unknownFields;
  }
  private ProductDTO(
      com.google.protobuf.CodedInputStream input,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws com.google.protobuf.InvalidProtocolBufferException {
    this();
    if (extensionRegistry == null) {
      throw new NullPointerException();
    }
    com.google.protobuf.UnknownFieldSet.Builder unknownFields =
        com.google.protobuf.UnknownFieldSet.newBuilder();
    try {
      boolean done = false;
      while (!done) {
        int tag = input.readTag();
        switch (tag) {
          case 0:
            done = true;
            break;
          case 10: {
            String s = input.readStringRequireUtf8();

            ratePlanCode_ = s;
            break;
          }
          case 18: {
            String s = input.readStringRequireUtf8();

            roomTypeCode_ = s;
            break;
          }
          case 8002: {
            TPAExtensionsDTO.Builder subBuilder = null;
            if (tpaExtensions_ != null) {
              subBuilder = tpaExtensions_.toBuilder();
            }
            tpaExtensions_ = input.readMessage(TPAExtensionsDTO.parser(), extensionRegistry);
            if (subBuilder != null) {
              subBuilder.mergeFrom(tpaExtensions_);
              tpaExtensions_ = subBuilder.buildPartial();
            }

            break;
          }
          default: {
            if (!parseUnknownField(
                input, unknownFields, extensionRegistry, tag)) {
              done = true;
            }
            break;
          }
        }
      }
    } catch (com.google.protobuf.InvalidProtocolBufferException e) {
      throw e.setUnfinishedMessage(this);
    } catch (java.io.IOException e) {
      throw new com.google.protobuf.InvalidProtocolBufferException(
          e).setUnfinishedMessage(this);
    } finally {
      this.unknownFields = unknownFields.build();
      makeExtensionsImmutable();
    }
  }
  public static final com.google.protobuf.Descriptors.Descriptor
      getDescriptor() {
    return ConnectivityProductsPB.internal_static_com_derbysoft_servreg_ProductDTO_descriptor;
  }

  @Override
  protected FieldAccessorTable
      internalGetFieldAccessorTable() {
    return ConnectivityProductsPB.internal_static_com_derbysoft_servreg_ProductDTO_fieldAccessorTable
        .ensureFieldAccessorsInitialized(
            ProductDTO.class, Builder.class);
  }

  public static final int RATEPLANCODE_FIELD_NUMBER = 1;
  private volatile Object ratePlanCode_;
  /**
   * <code>string ratePlanCode = 1;</code>
   * @return The ratePlanCode.
   */
  @Override
  public String getRatePlanCode() {
    Object ref = ratePlanCode_;
    if (ref instanceof String) {
      return (String) ref;
    } else {
      com.google.protobuf.ByteString bs = 
          (com.google.protobuf.ByteString) ref;
      String s = bs.toStringUtf8();
      ratePlanCode_ = s;
      return s;
    }
  }
  /**
   * <code>string ratePlanCode = 1;</code>
   * @return The bytes for ratePlanCode.
   */
  @Override
  public com.google.protobuf.ByteString
      getRatePlanCodeBytes() {
    Object ref = ratePlanCode_;
    if (ref instanceof String) {
      com.google.protobuf.ByteString b = 
          com.google.protobuf.ByteString.copyFromUtf8(
              (String) ref);
      ratePlanCode_ = b;
      return b;
    } else {
      return (com.google.protobuf.ByteString) ref;
    }
  }

  public static final int ROOMTYPECODE_FIELD_NUMBER = 2;
  private volatile Object roomTypeCode_;
  /**
   * <code>string roomTypeCode = 2;</code>
   * @return The roomTypeCode.
   */
  @Override
  public String getRoomTypeCode() {
    Object ref = roomTypeCode_;
    if (ref instanceof String) {
      return (String) ref;
    } else {
      com.google.protobuf.ByteString bs = 
          (com.google.protobuf.ByteString) ref;
      String s = bs.toStringUtf8();
      roomTypeCode_ = s;
      return s;
    }
  }
  /**
   * <code>string roomTypeCode = 2;</code>
   * @return The bytes for roomTypeCode.
   */
  @Override
  public com.google.protobuf.ByteString
      getRoomTypeCodeBytes() {
    Object ref = roomTypeCode_;
    if (ref instanceof String) {
      com.google.protobuf.ByteString b = 
          com.google.protobuf.ByteString.copyFromUtf8(
              (String) ref);
      roomTypeCode_ = b;
      return b;
    } else {
      return (com.google.protobuf.ByteString) ref;
    }
  }

  public static final int TPAEXTENSIONS_FIELD_NUMBER = 1000;
  private TPAExtensionsDTO tpaExtensions_;
  /**
   * <code>.com.derbysoft.servreg.TPAExtensionsDTO tpaExtensions = 1000;</code>
   * @return Whether the tpaExtensions field is set.
   */
  @Override
  public boolean hasTpaExtensions() {
    return tpaExtensions_ != null;
  }
  /**
   * <code>.com.derbysoft.servreg.TPAExtensionsDTO tpaExtensions = 1000;</code>
   * @return The tpaExtensions.
   */
  @Override
  public TPAExtensionsDTO getTpaExtensions() {
    return tpaExtensions_ == null ? TPAExtensionsDTO.getDefaultInstance() : tpaExtensions_;
  }
  /**
   * <code>.com.derbysoft.servreg.TPAExtensionsDTO tpaExtensions = 1000;</code>
   */
  @Override
  public TPAExtensionsDTOOrBuilder getTpaExtensionsOrBuilder() {
    return getTpaExtensions();
  }

  private byte memoizedIsInitialized = -1;
  @Override
  public final boolean isInitialized() {
    byte isInitialized = memoizedIsInitialized;
    if (isInitialized == 1) return true;
    if (isInitialized == 0) return false;

    memoizedIsInitialized = 1;
    return true;
  }

  @Override
  public void writeTo(com.google.protobuf.CodedOutputStream output)
                      throws java.io.IOException {
    if (!getRatePlanCodeBytes().isEmpty()) {
      com.google.protobuf.GeneratedMessageV3.writeString(output, 1, ratePlanCode_);
    }
    if (!getRoomTypeCodeBytes().isEmpty()) {
      com.google.protobuf.GeneratedMessageV3.writeString(output, 2, roomTypeCode_);
    }
    if (tpaExtensions_ != null) {
      output.writeMessage(1000, getTpaExtensions());
    }
    unknownFields.writeTo(output);
  }

  @Override
  public int getSerializedSize() {
    int size = memoizedSize;
    if (size != -1) return size;

    size = 0;
    if (!getRatePlanCodeBytes().isEmpty()) {
      size += com.google.protobuf.GeneratedMessageV3.computeStringSize(1, ratePlanCode_);
    }
    if (!getRoomTypeCodeBytes().isEmpty()) {
      size += com.google.protobuf.GeneratedMessageV3.computeStringSize(2, roomTypeCode_);
    }
    if (tpaExtensions_ != null) {
      size += com.google.protobuf.CodedOutputStream
        .computeMessageSize(1000, getTpaExtensions());
    }
    size += unknownFields.getSerializedSize();
    memoizedSize = size;
    return size;
  }

  @Override
  public boolean equals(final Object obj) {
    if (obj == this) {
     return true;
    }
    if (!(obj instanceof ProductDTO)) {
      return super.equals(obj);
    }
    ProductDTO other = (ProductDTO) obj;

    if (!getRatePlanCode()
        .equals(other.getRatePlanCode())) return false;
    if (!getRoomTypeCode()
        .equals(other.getRoomTypeCode())) return false;
    if (hasTpaExtensions() != other.hasTpaExtensions()) return false;
    if (hasTpaExtensions()) {
      if (!getTpaExtensions()
          .equals(other.getTpaExtensions())) return false;
    }
    if (!unknownFields.equals(other.unknownFields)) return false;
    return true;
  }

  @Override
  public int hashCode() {
    if (memoizedHashCode != 0) {
      return memoizedHashCode;
    }
    int hash = 41;
    hash = (19 * hash) + getDescriptor().hashCode();
    hash = (37 * hash) + RATEPLANCODE_FIELD_NUMBER;
    hash = (53 * hash) + getRatePlanCode().hashCode();
    hash = (37 * hash) + ROOMTYPECODE_FIELD_NUMBER;
    hash = (53 * hash) + getRoomTypeCode().hashCode();
    if (hasTpaExtensions()) {
      hash = (37 * hash) + TPAEXTENSIONS_FIELD_NUMBER;
      hash = (53 * hash) + getTpaExtensions().hashCode();
    }
    hash = (29 * hash) + unknownFields.hashCode();
    memoizedHashCode = hash;
    return hash;
  }

  public static ProductDTO parseFrom(
      java.nio.ByteBuffer data)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data);
  }
  public static ProductDTO parseFrom(
      java.nio.ByteBuffer data,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data, extensionRegistry);
  }
  public static ProductDTO parseFrom(
      com.google.protobuf.ByteString data)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data);
  }
  public static ProductDTO parseFrom(
      com.google.protobuf.ByteString data,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data, extensionRegistry);
  }
  public static ProductDTO parseFrom(byte[] data)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data);
  }
  public static ProductDTO parseFrom(
      byte[] data,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data, extensionRegistry);
  }
  public static ProductDTO parseFrom(java.io.InputStream input)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseWithIOException(PARSER, input);
  }
  public static ProductDTO parseFrom(
      java.io.InputStream input,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseWithIOException(PARSER, input, extensionRegistry);
  }
  public static ProductDTO parseDelimitedFrom(java.io.InputStream input)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseDelimitedWithIOException(PARSER, input);
  }
  public static ProductDTO parseDelimitedFrom(
      java.io.InputStream input,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseDelimitedWithIOException(PARSER, input, extensionRegistry);
  }
  public static ProductDTO parseFrom(
      com.google.protobuf.CodedInputStream input)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseWithIOException(PARSER, input);
  }
  public static ProductDTO parseFrom(
      com.google.protobuf.CodedInputStream input,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseWithIOException(PARSER, input, extensionRegistry);
  }

  @Override
  public Builder newBuilderForType() { return newBuilder(); }
  public static Builder newBuilder() {
    return DEFAULT_INSTANCE.toBuilder();
  }
  public static Builder newBuilder(ProductDTO prototype) {
    return DEFAULT_INSTANCE.toBuilder().mergeFrom(prototype);
  }
  @Override
  public Builder toBuilder() {
    return this == DEFAULT_INSTANCE
        ? new Builder() : new Builder().mergeFrom(this);
  }

  @Override
  protected Builder newBuilderForType(
      BuilderParent parent) {
    Builder builder = new Builder(parent);
    return builder;
  }
  /**
   * Protobuf type {@code com.derbysoft.servreg.ProductDTO}
   */
  public static final class Builder extends
      com.google.protobuf.GeneratedMessageV3.Builder<Builder> implements
      // @@protoc_insertion_point(builder_implements:com.derbysoft.servreg.ProductDTO)
      ProductDTOOrBuilder {
    public static final com.google.protobuf.Descriptors.Descriptor
        getDescriptor() {
      return ConnectivityProductsPB.internal_static_com_derbysoft_servreg_ProductDTO_descriptor;
    }

    @Override
    protected FieldAccessorTable
        internalGetFieldAccessorTable() {
      return ConnectivityProductsPB.internal_static_com_derbysoft_servreg_ProductDTO_fieldAccessorTable
          .ensureFieldAccessorsInitialized(
              ProductDTO.class, Builder.class);
    }

    // Construct using com.derbysoft.servreg.rpc.products.ProductDTO.newBuilder()
    private Builder() {
      maybeForceBuilderInitialization();
    }

    private Builder(
        BuilderParent parent) {
      super(parent);
      maybeForceBuilderInitialization();
    }
    private void maybeForceBuilderInitialization() {
      if (com.google.protobuf.GeneratedMessageV3
              .alwaysUseFieldBuilders) {
      }
    }
    @Override
    public Builder clear() {
      super.clear();
      ratePlanCode_ = "";

      roomTypeCode_ = "";

      if (tpaExtensionsBuilder_ == null) {
        tpaExtensions_ = null;
      } else {
        tpaExtensions_ = null;
        tpaExtensionsBuilder_ = null;
      }
      return this;
    }

    @Override
    public com.google.protobuf.Descriptors.Descriptor
        getDescriptorForType() {
      return ConnectivityProductsPB.internal_static_com_derbysoft_servreg_ProductDTO_descriptor;
    }

    @Override
    public ProductDTO getDefaultInstanceForType() {
      return ProductDTO.getDefaultInstance();
    }

    @Override
    public ProductDTO build() {
      ProductDTO result = buildPartial();
      if (!result.isInitialized()) {
        throw newUninitializedMessageException(result);
      }
      return result;
    }

    @Override
    public ProductDTO buildPartial() {
      ProductDTO result = new ProductDTO(this);
      result.ratePlanCode_ = ratePlanCode_;
      result.roomTypeCode_ = roomTypeCode_;
      if (tpaExtensionsBuilder_ == null) {
        result.tpaExtensions_ = tpaExtensions_;
      } else {
        result.tpaExtensions_ = tpaExtensionsBuilder_.build();
      }
      onBuilt();
      return result;
    }

    @Override
    public Builder clone() {
      return super.clone();
    }
    @Override
    public Builder setField(
        com.google.protobuf.Descriptors.FieldDescriptor field,
        Object value) {
      return super.setField(field, value);
    }
    @Override
    public Builder clearField(
        com.google.protobuf.Descriptors.FieldDescriptor field) {
      return super.clearField(field);
    }
    @Override
    public Builder clearOneof(
        com.google.protobuf.Descriptors.OneofDescriptor oneof) {
      return super.clearOneof(oneof);
    }
    @Override
    public Builder setRepeatedField(
        com.google.protobuf.Descriptors.FieldDescriptor field,
        int index, Object value) {
      return super.setRepeatedField(field, index, value);
    }
    @Override
    public Builder addRepeatedField(
        com.google.protobuf.Descriptors.FieldDescriptor field,
        Object value) {
      return super.addRepeatedField(field, value);
    }
    @Override
    public Builder mergeFrom(com.google.protobuf.Message other) {
      if (other instanceof ProductDTO) {
        return mergeFrom((ProductDTO)other);
      } else {
        super.mergeFrom(other);
        return this;
      }
    }

    public Builder mergeFrom(ProductDTO other) {
      if (other == ProductDTO.getDefaultInstance()) return this;
      if (!other.getRatePlanCode().isEmpty()) {
        ratePlanCode_ = other.ratePlanCode_;
        onChanged();
      }
      if (!other.getRoomTypeCode().isEmpty()) {
        roomTypeCode_ = other.roomTypeCode_;
        onChanged();
      }
      if (other.hasTpaExtensions()) {
        mergeTpaExtensions(other.getTpaExtensions());
      }
      this.mergeUnknownFields(other.unknownFields);
      onChanged();
      return this;
    }

    @Override
    public final boolean isInitialized() {
      return true;
    }

    @Override
    public Builder mergeFrom(
        com.google.protobuf.CodedInputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws java.io.IOException {
      ProductDTO parsedMessage = null;
      try {
        parsedMessage = PARSER.parsePartialFrom(input, extensionRegistry);
      } catch (com.google.protobuf.InvalidProtocolBufferException e) {
        parsedMessage = (ProductDTO) e.getUnfinishedMessage();
        throw e.unwrapIOException();
      } finally {
        if (parsedMessage != null) {
          mergeFrom(parsedMessage);
        }
      }
      return this;
    }

    private Object ratePlanCode_ = "";
    /**
     * <code>string ratePlanCode = 1;</code>
     * @return The ratePlanCode.
     */
    public String getRatePlanCode() {
      Object ref = ratePlanCode_;
      if (!(ref instanceof String)) {
        com.google.protobuf.ByteString bs =
            (com.google.protobuf.ByteString) ref;
        String s = bs.toStringUtf8();
        ratePlanCode_ = s;
        return s;
      } else {
        return (String) ref;
      }
    }
    /**
     * <code>string ratePlanCode = 1;</code>
     * @return The bytes for ratePlanCode.
     */
    public com.google.protobuf.ByteString
        getRatePlanCodeBytes() {
      Object ref = ratePlanCode_;
      if (ref instanceof String) {
        com.google.protobuf.ByteString b = 
            com.google.protobuf.ByteString.copyFromUtf8(
                (String) ref);
        ratePlanCode_ = b;
        return b;
      } else {
        return (com.google.protobuf.ByteString) ref;
      }
    }
    /**
     * <code>string ratePlanCode = 1;</code>
     * @param value The ratePlanCode to set.
     * @return This builder for chaining.
     */
    public Builder setRatePlanCode(
        String value) {
      if (value == null) {
    throw new NullPointerException();
  }
  
      ratePlanCode_ = value;
      onChanged();
      return this;
    }
    /**
     * <code>string ratePlanCode = 1;</code>
     * @return This builder for chaining.
     */
    public Builder clearRatePlanCode() {
      
      ratePlanCode_ = getDefaultInstance().getRatePlanCode();
      onChanged();
      return this;
    }
    /**
     * <code>string ratePlanCode = 1;</code>
     * @param value The bytes for ratePlanCode to set.
     * @return This builder for chaining.
     */
    public Builder setRatePlanCodeBytes(
        com.google.protobuf.ByteString value) {
      if (value == null) {
    throw new NullPointerException();
  }
  checkByteStringIsUtf8(value);
      
      ratePlanCode_ = value;
      onChanged();
      return this;
    }

    private Object roomTypeCode_ = "";
    /**
     * <code>string roomTypeCode = 2;</code>
     * @return The roomTypeCode.
     */
    public String getRoomTypeCode() {
      Object ref = roomTypeCode_;
      if (!(ref instanceof String)) {
        com.google.protobuf.ByteString bs =
            (com.google.protobuf.ByteString) ref;
        String s = bs.toStringUtf8();
        roomTypeCode_ = s;
        return s;
      } else {
        return (String) ref;
      }
    }
    /**
     * <code>string roomTypeCode = 2;</code>
     * @return The bytes for roomTypeCode.
     */
    public com.google.protobuf.ByteString
        getRoomTypeCodeBytes() {
      Object ref = roomTypeCode_;
      if (ref instanceof String) {
        com.google.protobuf.ByteString b = 
            com.google.protobuf.ByteString.copyFromUtf8(
                (String) ref);
        roomTypeCode_ = b;
        return b;
      } else {
        return (com.google.protobuf.ByteString) ref;
      }
    }
    /**
     * <code>string roomTypeCode = 2;</code>
     * @param value The roomTypeCode to set.
     * @return This builder for chaining.
     */
    public Builder setRoomTypeCode(
        String value) {
      if (value == null) {
    throw new NullPointerException();
  }
  
      roomTypeCode_ = value;
      onChanged();
      return this;
    }
    /**
     * <code>string roomTypeCode = 2;</code>
     * @return This builder for chaining.
     */
    public Builder clearRoomTypeCode() {
      
      roomTypeCode_ = getDefaultInstance().getRoomTypeCode();
      onChanged();
      return this;
    }
    /**
     * <code>string roomTypeCode = 2;</code>
     * @param value The bytes for roomTypeCode to set.
     * @return This builder for chaining.
     */
    public Builder setRoomTypeCodeBytes(
        com.google.protobuf.ByteString value) {
      if (value == null) {
    throw new NullPointerException();
  }
  checkByteStringIsUtf8(value);
      
      roomTypeCode_ = value;
      onChanged();
      return this;
    }

    private TPAExtensionsDTO tpaExtensions_;
    private com.google.protobuf.SingleFieldBuilderV3<
        TPAExtensionsDTO, TPAExtensionsDTO.Builder, TPAExtensionsDTOOrBuilder> tpaExtensionsBuilder_;
    /**
     * <code>.com.derbysoft.servreg.TPAExtensionsDTO tpaExtensions = 1000;</code>
     * @return Whether the tpaExtensions field is set.
     */
    public boolean hasTpaExtensions() {
      return tpaExtensionsBuilder_ != null || tpaExtensions_ != null;
    }
    /**
     * <code>.com.derbysoft.servreg.TPAExtensionsDTO tpaExtensions = 1000;</code>
     * @return The tpaExtensions.
     */
    public TPAExtensionsDTO getTpaExtensions() {
      if (tpaExtensionsBuilder_ == null) {
        return tpaExtensions_ == null ? TPAExtensionsDTO.getDefaultInstance() : tpaExtensions_;
      } else {
        return tpaExtensionsBuilder_.getMessage();
      }
    }
    /**
     * <code>.com.derbysoft.servreg.TPAExtensionsDTO tpaExtensions = 1000;</code>
     */
    public Builder setTpaExtensions(TPAExtensionsDTO value) {
      if (tpaExtensionsBuilder_ == null) {
        if (value == null) {
          throw new NullPointerException();
        }
        tpaExtensions_ = value;
        onChanged();
      } else {
        tpaExtensionsBuilder_.setMessage(value);
      }

      return this;
    }
    /**
     * <code>.com.derbysoft.servreg.TPAExtensionsDTO tpaExtensions = 1000;</code>
     */
    public Builder setTpaExtensions(
        TPAExtensionsDTO.Builder builderForValue) {
      if (tpaExtensionsBuilder_ == null) {
        tpaExtensions_ = builderForValue.build();
        onChanged();
      } else {
        tpaExtensionsBuilder_.setMessage(builderForValue.build());
      }

      return this;
    }
    /**
     * <code>.com.derbysoft.servreg.TPAExtensionsDTO tpaExtensions = 1000;</code>
     */
    public Builder mergeTpaExtensions(TPAExtensionsDTO value) {
      if (tpaExtensionsBuilder_ == null) {
        if (tpaExtensions_ != null) {
          tpaExtensions_ =
            TPAExtensionsDTO.newBuilder(tpaExtensions_).mergeFrom(value).buildPartial();
        } else {
          tpaExtensions_ = value;
        }
        onChanged();
      } else {
        tpaExtensionsBuilder_.mergeFrom(value);
      }

      return this;
    }
    /**
     * <code>.com.derbysoft.servreg.TPAExtensionsDTO tpaExtensions = 1000;</code>
     */
    public Builder clearTpaExtensions() {
      if (tpaExtensionsBuilder_ == null) {
        tpaExtensions_ = null;
        onChanged();
      } else {
        tpaExtensions_ = null;
        tpaExtensionsBuilder_ = null;
      }

      return this;
    }
    /**
     * <code>.com.derbysoft.servreg.TPAExtensionsDTO tpaExtensions = 1000;</code>
     */
    public TPAExtensionsDTO.Builder getTpaExtensionsBuilder() {
      
      onChanged();
      return getTpaExtensionsFieldBuilder().getBuilder();
    }
    /**
     * <code>.com.derbysoft.servreg.TPAExtensionsDTO tpaExtensions = 1000;</code>
     */
    public TPAExtensionsDTOOrBuilder getTpaExtensionsOrBuilder() {
      if (tpaExtensionsBuilder_ != null) {
        return tpaExtensionsBuilder_.getMessageOrBuilder();
      } else {
        return tpaExtensions_ == null ?
            TPAExtensionsDTO.getDefaultInstance() : tpaExtensions_;
      }
    }
    /**
     * <code>.com.derbysoft.servreg.TPAExtensionsDTO tpaExtensions = 1000;</code>
     */
    private com.google.protobuf.SingleFieldBuilderV3<
        TPAExtensionsDTO, TPAExtensionsDTO.Builder, TPAExtensionsDTOOrBuilder>
        getTpaExtensionsFieldBuilder() {
      if (tpaExtensionsBuilder_ == null) {
        tpaExtensionsBuilder_ = new com.google.protobuf.SingleFieldBuilderV3<
            TPAExtensionsDTO, TPAExtensionsDTO.Builder, TPAExtensionsDTOOrBuilder>(
                getTpaExtensions(),
                getParentForChildren(),
                isClean());
        tpaExtensions_ = null;
      }
      return tpaExtensionsBuilder_;
    }
    @Override
    public final Builder setUnknownFields(
        final com.google.protobuf.UnknownFieldSet unknownFields) {
      return super.setUnknownFields(unknownFields);
    }

    @Override
    public final Builder mergeUnknownFields(
        final com.google.protobuf.UnknownFieldSet unknownFields) {
      return super.mergeUnknownFields(unknownFields);
    }


    // @@protoc_insertion_point(builder_scope:com.derbysoft.servreg.ProductDTO)
  }

  // @@protoc_insertion_point(class_scope:com.derbysoft.servreg.ProductDTO)
  private static final ProductDTO DEFAULT_INSTANCE;
  static {
    DEFAULT_INSTANCE = new ProductDTO();
  }

  public static ProductDTO getDefaultInstance() {
    return DEFAULT_INSTANCE;
  }

  private static final com.google.protobuf.Parser<ProductDTO>
      PARSER = new com.google.protobuf.AbstractParser<ProductDTO>() {
    @Override
    public ProductDTO parsePartialFrom(
        com.google.protobuf.CodedInputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws com.google.protobuf.InvalidProtocolBufferException {
      return new ProductDTO(input, extensionRegistry);
    }
  };

  public static com.google.protobuf.Parser<ProductDTO> parser() {
    return PARSER;
  }

  @Override
  public com.google.protobuf.Parser<ProductDTO> getParserForType() {
    return PARSER;
  }

  @Override
  public ProductDTO getDefaultInstanceForType() {
    return DEFAULT_INSTANCE;
  }

}

