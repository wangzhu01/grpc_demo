// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: products_service.proto

package com.derbysoft.servreg.rpc.products;

public interface ProductsRQOrBuilder extends
    // @@protoc_insertion_point(interface_extends:com.derbysoft.servreg.ProductsRQ)
    com.google.protobuf.MessageOrBuilder {

  /**
   * <code>string channelCode = 1;</code>
   * @return The channelCode.
   */
  String getChannelCode();
  /**
   * <code>string channelCode = 1;</code>
   * @return The bytes for channelCode.
   */
  com.google.protobuf.ByteString
      getChannelCodeBytes();

  /**
   * <code>string supplierCode = 2;</code>
   * @return The supplierCode.
   */
  String getSupplierCode();
  /**
   * <code>string supplierCode = 2;</code>
   * @return The bytes for supplierCode.
   */
  com.google.protobuf.ByteString
      getSupplierCodeBytes();

  /**
   * <code>string hotelCode = 3;</code>
   * @return The hotelCode.
   */
  String getHotelCode();
  /**
   * <code>string hotelCode = 3;</code>
   * @return The bytes for hotelCode.
   */
  com.google.protobuf.ByteString
      getHotelCodeBytes();

  /**
   * <code>repeated string ratePlans = 4;</code>
   * @return A list containing the ratePlans.
   */
  java.util.List<String>
      getRatePlansList();
  /**
   * <code>repeated string ratePlans = 4;</code>
   * @return The count of ratePlans.
   */
  int getRatePlansCount();
  /**
   * <code>repeated string ratePlans = 4;</code>
   * @param index The index of the element to return.
   * @return The ratePlans at the given index.
   */
  String getRatePlans(int index);
  /**
   * <code>repeated string ratePlans = 4;</code>
   * @param index The index of the value to return.
   * @return The bytes of the ratePlans at the given index.
   */
  com.google.protobuf.ByteString
      getRatePlansBytes(int index);

  /**
   * <code>repeated string roomTypes = 5;</code>
   * @return A list containing the roomTypes.
   */
  java.util.List<String>
      getRoomTypesList();
  /**
   * <code>repeated string roomTypes = 5;</code>
   * @return The count of roomTypes.
   */
  int getRoomTypesCount();
  /**
   * <code>repeated string roomTypes = 5;</code>
   * @param index The index of the element to return.
   * @return The roomTypes at the given index.
   */
  String getRoomTypes(int index);
  /**
   * <code>repeated string roomTypes = 5;</code>
   * @param index The index of the value to return.
   * @return The bytes of the roomTypes at the given index.
   */
  com.google.protobuf.ByteString
      getRoomTypesBytes(int index);

  /**
   * <code>.com.derbysoft.servreg.TPAExtensionsDTO tpaExtensions = 1000;</code>
   * @return Whether the tpaExtensions field is set.
   */
  boolean hasTpaExtensions();
  /**
   * <code>.com.derbysoft.servreg.TPAExtensionsDTO tpaExtensions = 1000;</code>
   * @return The tpaExtensions.
   */
  TPAExtensionsDTO getTpaExtensions();
  /**
   * <code>.com.derbysoft.servreg.TPAExtensionsDTO tpaExtensions = 1000;</code>
   */
  TPAExtensionsDTOOrBuilder getTpaExtensionsOrBuilder();
}
