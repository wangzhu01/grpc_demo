// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: products_service.proto

package com.derbysoft.servreg.rpc.products;

public interface TraceInfoOrBuilder extends
    // @@protoc_insertion_point(interface_extends:com.derbysoft.servreg.TraceInfo)
    com.google.protobuf.MessageOrBuilder {

  /**
   * <code>int64 traceId = 1;</code>
   * @return The traceId.
   */
  long getTraceId();

  /**
   * <code>int64 spanId = 2;</code>
   * @return The spanId.
   */
  long getSpanId();

  /**
   * <code>int64 parentSpanId = 3;</code>
   * @return The parentSpanId.
   */
  long getParentSpanId();

  /**
   * <code>bool sampled = 4;</code>
   * @return The sampled.
   */
  boolean getSampled();

  /**
   * <code>int32 flags = 5;</code>
   * @return The flags.
   */
  int getFlags();

  /**
   * <code>string peerHost = 6;</code>
   * @return The peerHost.
   */
  String getPeerHost();
  /**
   * <code>string peerHost = 6;</code>
   * @return The bytes for peerHost.
   */
  com.google.protobuf.ByteString
      getPeerHostBytes();

  /**
   * <code>int32 peerPort = 7;</code>
   * @return The peerPort.
   */
  int getPeerPort();

  /**
   * <code>.com.derbysoft.servreg.TPAExtensionsDTO tpaExtensions = 1000;</code>
   * @return Whether the tpaExtensions field is set.
   */
  boolean hasTpaExtensions();
  /**
   * <code>.com.derbysoft.servreg.TPAExtensionsDTO tpaExtensions = 1000;</code>
   * @return The tpaExtensions.
   */
  TPAExtensionsDTO getTpaExtensions();
  /**
   * <code>.com.derbysoft.servreg.TPAExtensionsDTO tpaExtensions = 1000;</code>
   */
  TPAExtensionsDTOOrBuilder getTpaExtensionsOrBuilder();
}
