package client;

import io.grpc.*;

import java.util.logging.Logger;

public class interceptor implements ClientInterceptor {

    private static final Logger logger = Logger.getLogger(interceptor.class.getName());

    static final Metadata.Key<String> CUSTOM_HEADER_KEY =
            Metadata.Key.of("custom_client_header_key", Metadata.ASCII_STRING_MARSHALLER);

    static final Metadata.Key<String> URI =
            Metadata.Key.of("uri", Metadata.ASCII_STRING_MARSHALLER);

    static final Metadata.Key<String> TASK_ID =
            Metadata.Key.of("taskId", Metadata.ASCII_STRING_MARSHALLER);

    static String uri;
    static String taskId;

    @Override
    public <ReqT, RespT> ClientCall<ReqT, RespT> interceptCall(final MethodDescriptor<ReqT, RespT> methodDescriptor,
                                                               CallOptions callOptions, final Channel channel) {
        return new ForwardingClientCall.SimpleForwardingClientCall<ReqT, RespT>(channel.newCall(methodDescriptor, callOptions)) {
            @Override
            public void start(Listener<RespT> responseListener, Metadata headers) {

                logger.info(methodDescriptor.getFullMethodName());
                logger.info(methodDescriptor.getServiceName());

                super.start(new ForwardingClientCallListener.SimpleForwardingClientCallListener<RespT>(responseListener) {
                    @Override
                    public void onHeaders(Metadata headers) {
                        /**
                         * if you don't need receive header from server,
                         * you can use {@link io.grpc.stub.MetadataUtils#attachHeaders}
                         * directly to send header
                         */
                        logger.info("header received from server:" + headers);
                        super.onHeaders(headers);
                    }
                }, headers);
            }
            @Override
            public void sendMessage(ReqT message) {
                this.delegate().sendMessage(message);
            }

        };
    }
}
