package client;

import com.derbysoft.servreg.rpc.products.*;
import extension.servicediscovery.DerbySoftSGCResolverProvider;
import io.grpc.*;

import java.util.concurrent.TimeUnit;

public class clientDemo {

    //使用main方法来测试client端
    public static void main(String[] args) throws Exception {

        clientDemo clientDemo = new clientDemo();

        try {
            //基于gRPC远程调用对应的方法
            //DerbySoftSGC:/host/serviceId
            clientDemo.remoteCall("DerbySoftSGC:/127.0.0.1:10080/products:us-west-2::::");

        } finally {

        }
    }

    /**
     * 基于gRPC框架的使用步骤，进行远程调用
     */
    public void remoteCall(String destination) throws InterruptedException {

        GetProductsRequest getProductsRequest = GetProductsRequest.newBuilder().
                setProductsRQ(ProductsRQ.newBuilder().setChannelCode("channelCode").build()).
        setHeader(RequestMessage.newBuilder().setHeader(Header.newBuilder().setUri("uri").build()).build()).build();
        GetProductsResponse getProductsResponse;
        GetProductsResponse getProductsResponse2;
        GetProductsResponse getProductsResponse3;
        GetProductsResponse getProductsResponse4;
        GetProductsResponse getProductsResponse5;
        GetProductsResponse getProductsResponse6;
        // 指定负载均衡策略
        ManagedChannel channel = ManagedChannelBuilder.forTarget(destination)
                .defaultLoadBalancingPolicy("pick_first")
                .keepAliveTime(20,TimeUnit.SECONDS)
                .keepAliveTimeout(20,TimeUnit.NANOSECONDS)
                .idleTimeout(50,TimeUnit.SECONDS)
                .enableRetry().usePlaintext().build();
        try {


            NameResolverRegistry.getDefaultRegistry().register(new DerbySoftSGCResolverProvider());

            //LoadBalancerRegistry.getDefaultRegistry().register(new GrpclbLoadBalancerProvider());
            // 基于访问地址 创建通道
            Channel inChannel = ClientInterceptors.intercept(channel,new interceptor());

            // 利用通道 创建一个桩（Stub）对象
            ConnectivityProductsGrpc.ConnectivityProductsBlockingStub blockingStub = ConnectivityProductsGrpc.newBlockingStub(inChannel);

            //通过桩对象来调用远程方法
            getProductsResponse = blockingStub.getProducts(getProductsRequest);
            getProductsResponse2 = blockingStub.getProducts(getProductsRequest);
            getProductsResponse3 = blockingStub.getProducts(getProductsRequest);
            getProductsResponse4 = blockingStub.getProducts(getProductsRequest);
            getProductsResponse5 = blockingStub.getProducts(getProductsRequest);
            getProductsResponse6 = blockingStub.getProducts(getProductsRequest);

        } catch (StatusRuntimeException e) {
            return;
        }finally {
            channel.shutdownNow().awaitTermination(5, TimeUnit.SECONDS);
        }

        System.out.println("client getProducts {}：\n" + getProductsResponse.getProductsRS().getProducts(0));
        System.out.println("client getProducts2 {}：\n" + getProductsResponse2.getProductsRS().getProducts(0));
        System.out.println("client getProducts3 {}：\n" + getProductsResponse3.getProductsRS().getProducts(0));
        System.out.println("client getProducts4 {}：\n" + getProductsResponse4.getProductsRS().getProducts(0));
        System.out.println("client getProducts5 {}：\n" + getProductsResponse5.getProductsRS().getProducts(0));
        System.out.println("client getProducts6 {}：\n" + getProductsResponse6.getProductsRS().getProducts(0));

    }

}
