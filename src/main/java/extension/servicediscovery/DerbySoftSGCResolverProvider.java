package extension.servicediscovery;

import com.google.common.base.Preconditions;
import io.grpc.Attributes;
import io.grpc.NameResolver;
import io.grpc.NameResolverProvider;
import io.grpc.internal.GrpcUtil;

import java.net.URI;

public class DerbySoftSGCResolverProvider extends NameResolverProvider {

    private static final String SCHEME = "DerbySoftSGC";

    @Override
    public NameResolver newNameResolver(URI targetUri, Attributes params) {
        if (SCHEME.equals(targetUri.getScheme())) {
            String targetPath = Preconditions.checkNotNull(targetUri.getPath(), "targetPath");
            Preconditions.checkArgument(targetPath.startsWith("/"),
                    "the path component (%s) of the target (%s) must start with '/'", targetPath, targetUri);
            String sgcAddress = targetPath.substring(1,targetPath.lastIndexOf("/"));
            String ServiceId = targetPath.substring(targetPath.lastIndexOf("/")+1);

            return new DerbySoftSGCResolver(targetUri, sgcAddress, ServiceId,params, GrpcUtil.TIMER_SERVICE,
                    GrpcUtil.SHARED_CHANNEL_EXECUTOR);
        } else {
            return null;
        }

    }

    @Override
    protected boolean isAvailable() {
        return true;
    }

    @Override
    protected int priority() {
        return 5;
    }

    @Override
    public String getDefaultScheme() {
        return SCHEME;
    }
}
