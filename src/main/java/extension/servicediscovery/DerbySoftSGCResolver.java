package extension.servicediscovery;

import com.derbysoft.servreg.client.ClientBuilder;
import com.derbysoft.servreg.client.api.ClientResult;
import com.derbysoft.servreg.client.api.ServiceDiscoveryClient;
import com.derbysoft.servreg.registry.ServiceRegistryBuilder;
import com.google.common.base.Preconditions;
import io.grpc.Attributes;
import io.grpc.EquivalentAddressGroup;
import io.grpc.NameResolver;
import io.grpc.internal.SharedResourceHolder;

import java.net.InetSocketAddress;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executor;
import java.util.concurrent.ScheduledExecutorService;

public class DerbySoftSGCResolver extends NameResolver {

    private Map<String, List<String>> serviceProviderMap = new ConcurrentHashMap<String, List<String>>();

    private final SharedResourceHolder.Resource<ScheduledExecutorService> timerServiceResource;
    private final SharedResourceHolder.Resource<Executor> executorResource;
    //@GuardedBy("this")
    private boolean shutdown;
    //@GuardedBy("this")
    private ScheduledExecutorService timerService;
    //@GuardedBy("this")
    private Executor executor;
    //@GuardedBy("this")
    private boolean resolving;
    //@GuardedBy("this")
    private volatile Listener listener;

    //private boolean resolving;
    private String sgcAddress;

    private String serviceId;

    private ServiceDiscoveryClient defaultServiceDiscoveryClient;


    private Object lock = new Object();


    public DerbySoftSGCResolver(URI targetUri, String sgcAddress, String serviceId, Attributes params, SharedResourceHolder.Resource<ScheduledExecutorService> timerServiceResource,
                                SharedResourceHolder.Resource<Executor> executorResource) {

        this.timerServiceResource = timerServiceResource;
        this.executorResource = executorResource;
        this.sgcAddress = sgcAddress;
        this.serviceId = serviceId;

    }

    @Override
    public String getServiceAuthority() {
        return "none";
    }

    @Override
    public void shutdown() {
        shutdown = true;
        if (timerService != null) {
            timerService = SharedResourceHolder.release(timerServiceResource, timerService);
        }
        if (executor != null) {
            executor = SharedResourceHolder.release(executorResource, executor);
        }
    }

    /**
     * resolver start
     * @param listener
     */
    @Override
    public void start(Listener2 listener) {
        Preconditions.checkState(this.listener == null, "already started");
        timerService = SharedResourceHolder.get(timerServiceResource);
        executor = SharedResourceHolder.get(executorResource);
        this.listener = Preconditions.checkNotNull(listener, "listener");
        resolve();
    }


    private void resolve() {
        if (resolving || shutdown) {
            return;
        }
        executor.execute(resolutionRunnable);
    }

    private final Runnable resolutionRunnable = new Runnable() {
        @Override
        public void run() {
            synchronized (lock) {
                resolveServerFunWithLock();
            }
        }
    };

    private void resolveServerFunWithLock() {

        applyFilter();

        Listener savedListener = listener;
        List<EquivalentAddressGroup> servers = new ArrayList();
        defaultServiceDiscoveryClient = ClientBuilder.newBuilder().withRegistryBuilder(ServiceRegistryBuilder.newBuilder().withAddresses(sgcAddress)).buildDiscoveryClient();
        ClientResult<List<String>> resp = defaultServiceDiscoveryClient.getEndpoints(serviceId);
        if (!resp.hasError()) {
            //serviceProviderMap.put(serviceId,resp.getResult());
            for (String endpoint : resp.getResult()) {
                InetSocketAddress inetSocketAddr = new InetSocketAddress(endpoint.substring(0, endpoint.indexOf(":")), Integer.valueOf(endpoint.substring(endpoint.indexOf(":") + 1)));
                servers.add(new EquivalentAddressGroup(inetSocketAddr));
            }
        }
        savedListener.onAddresses(servers, Attributes.EMPTY);
    }

    /**
     * 过滤器，此处实现自定义负载均衡
     */
    private void applyFilter() {
    }


}
